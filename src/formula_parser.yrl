Nonterminals formula.
Terminals '(' ')' 'not' 'or' 'and' 'implies' atom.
Rootsymbol formula.

formula -> atom : extract_token('$1').
formula -> 'not' formula : {extract_token('$1'), '$2'}.
formula -> '(' formula 'and' formula ')' :
   {'$2', extract_token('$3'), '$4'}.
formula -> '(' formula 'or' formula ')' :
   {'$2', extract_token('$3'), '$4'}.
formula -> '(' formula 'implies' formula ')' :
   {'$2', extract_token('$3'), '$4'}.

Erlang code.

extract_token({_Token, _Line, Value}) -> Value.
