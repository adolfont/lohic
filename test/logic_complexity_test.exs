defmodule LohicComplexityTest do
  use ExUnit.Case
  import Lohic

  test "complexidade de :p é 1" do
    assert complexidade(:p) == 1
  end

  test "complexidade de {:nao, :p} é 2" do
    assert complexidade({:nao, :p}) == 2
  end

  test "complexidade de {:p, :e, :q} é 3" do
    assert complexidade({:p, :e, :q}) == 3
  end

  test "complexidade de {:p, :e, {:q, :implica, {:nao, :z}}} é 6" do
    assert complexidade({:p, :e, {:q, :implica, {:nao, :z}}}) == 6
  end
end
