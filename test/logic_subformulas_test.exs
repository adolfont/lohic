defmodule LohicSubformulasTest do
  use ExUnit.Case
  # import Lohic
  import LohicSubformulas

  test "o conjunto de subfórmulas de :p é [:p]" do
    assert subformulas(:p) == MapSet.new([:p])
  end

  test "o conjunto de subfórmulas de :q é [:q]" do
    assert subformulas(:q) == MapSet.new([:q])
  end

  test "o conjunto de subfórmulas de !!p é [p, !p, !!p]" do
    assert subformulas({:nao, {:nao, :p}}) == MapSet.new([:p, {:nao, :p}, {:nao, {:nao, :p}}])
  end

  test "o conjunto de subfórmulas de (p&p) é [p, p&p]" do
    assert subformulas({:p, :e, :p}) == MapSet.new([:p, {:p, :e, :p}])
  end

  test "o conjunto de subfórmulas de (p|!p) é [p, !p, p|!p]" do
    assert subformulas({:p, :ou, {:nao, :p}}) ==
             MapSet.new([:p, {:nao, :p}, {:p, :ou, {:nao, :p}}])
  end

  test "o conjunto de subfórmulas de (p->!q) é [p, q, !q, p->!q]" do
    assert subformulas({:p, :implica, {:nao, :q}}) ==
             MapSet.new([:p, :q, {:nao, :q}, {:p, :implica, {:nao, :q}}])
  end
end
