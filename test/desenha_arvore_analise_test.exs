defmodule ArvoreAnaliseTeste do
  use ExUnit.Case
  import LohicArvore

  @result_a_e_b [" e ", "a b"]
  @result_a33_e_b456 ["   eoi    ", "a33   b456"]

  test "a complexidade de a é 1" do
    assert informacoes_formula(:a).complexidade == 1
  end

  test "a largura de a é 1" do
    assert informacoes_formula(:a).largura == 1
  end

  test "a altura de a é 1" do
    assert informacoes_formula(:a).altura == 1
  end

  test "as informacoes de a&!b são ..." do
    assert informacoes_formula({:a, :e, {:nao, :b}}) == %{altura: 3, complexidade: 4, largura: 5}
  end

  test "testes do coloca" do
    assert coloca([" "], 0, 0, "a") == ["a"]
  end

  test "testes do colocaVarios" do
    assert colocaVarios(["   c"], 0, 1, "ab") == [" abc"]
    assert colocaVarios(["   c", "    "], 1, 2, "ab") == ["   c", "  ab"]
  end

  test "a árvore de análise de a12 é a12" do
    assert arvore(:a12) == ["a12"]
  end

  test "a árvore de análise de a12ee é a12ee" do
    assert arvore(:a12ee) == ["a12ee"]
  end

  test "a árvore de análise de {:nao, :a}} é 'nao' 'a  '" do
    assert arvore({:nao, :a}) == ["nao", "a  "]
  end

  test "a árvore de análise de a&b é @result_a_e_b" do
    assert arvore({:a, :e, :b}) == @result_a_e_b
  end

  test "a árvore de análise de a33&b456 é @result_a33_e_b456" do
    assert arvore({:a33, :eoi, :b456}) == @result_a33_e_b456
  end

  test "a árvore de análise de !(a&b) é ..." do
    assert arvore({:nao, {:a, :e, :b}}) == ["nao", " e ", "a b"]
  end

  test "a árvore de análise de !(a&&&b) é ..." do
    assert arvore({:not, {:a, :and, :b}}) == [" not ", " and ", "a   b"]
  end

  test "a árvore de análise de !(a->b) é ..." do
    assert arvore({:nao, {:a, :implica, :b}}) == ["   nao   ", " implica ", "a       b"]
  end

  test "a árvore de análise de !(p|(q->q)) é ..." do
    assert arvore({:not, {:p, :or, {:q, :implies, :q}}}) ==
             ["not         ", " or         ", "p   implies ", "   q       q"]
  end

  test "a árvore de análise de !(q->quota) é ..." do
    assert arvore({:nononono, {:q, :implies, :quota}}) ==
             ["nononono     ", " implies     ", "q       quota"]
  end

  test "a árvore de análise de !(p|!(q->q)) é ..." do
    assert arvore({:not, {:p, :oreo, {:nononono, {:q, :implies, :quota}}}}) ==
             [
               " not              ",
               " oreo             ",
               "p    nononono     ",
               "      implies     ",
               "     q       quota"
             ]
  end

  # Teste iex -S mix
  # for i<-LohicArvore.arvore(  ), do: IO.puts(i)
end
