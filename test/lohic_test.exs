defmodule LohicTest do
  use ExUnit.Case
  import Lohic
  doctest Lohic

  @lpc {[:p, :q, :r], %{:nao => 1, :e => 2, :ou => 2, :implica => 2}}

  test "A fórmula :p é bem formada" do
    assert formula_bem_formada?(:p, @lpc)
  end

  test "A fórmula :s NÃO é bem formada" do
    refute formula_bem_formada?(:s, @lpc)
  end

  test "A fórmula 1 NÃO é bem formada" do
    refute formula_bem_formada?(1, @lpc)
  end

  test "A fórmula {:nao, :p}  é bem formada" do
    assert formula_bem_formada?({:nao, :p}, @lpc)
  end

  test "A fórmula {:oi, :p} NÃO é bem formada" do
    refute formula_bem_formada?({:oi, :p}, @lpc)
  end

  test "A fórmula {:p, :e, :q}  é bem formada" do
    assert formula_bem_formada?({:p, :e, :q}, @lpc)
  end

  test "A fórmula {:p, :ou, :q}  é bem formada" do
    assert formula_bem_formada?({:p, :ou, :q}, @lpc)
  end

  test "A fórmula {:p, :implica, :q}  é bem formada" do
    assert formula_bem_formada?({:p, :implica, :q}, @lpc)
  end

  test "A fórmula {:p1, :implica, :q} NÃO é bem formada" do
    refute formula_bem_formada?({:p1, :implica, :q}, @lpc)
  end

  test "A fórmula {:p, :implica, :q, :e, :r} NÃO é bem formada" do
    refute formula_bem_formada?({:p, :implica, :q, :e, :r}, @lpc)
  end

  test "A fórmula {:p, :implica, {:q, :e, :r}} é bem formada" do
    assert formula_bem_formada?({:p, :implica, {:q, :e, :r}}, @lpc)
  end

  test "A fórmula {:p, :implica, {:q, :e, {:nao, :r}}} é bem formada" do
    assert formula_bem_formada?({:p, :implica, {:q, :e, {:nao, :r}}}, @lpc)
  end
end
