# Algumas observações sobre a implementação da árvore de análise.

A entrada é uma fórmula.
A saída é a árvore em modo texto.

Exemplos:

(A1)
arvore(:p)
p

(A2)
arvore({:nao, :p})
nao
p

Ou seja, o conectivo "não" exige que sua subfórmula fique logo abaixo.

(A3)
arvore({:nao, {:nao, :p123}})
nao
nao
p123

O caso acima é interessante.
A largura da árvore é 4 caracteres, por conta da largura de p123.

(A4)
arvore({:p123, :e, :q4556})
    e
p123 q4556

O cuidado é sempre para não encavalar, ou seja, não escrever uma subfórmula em cima da outra. Para isso, o algoritmo que será implementado não será muito eficiente no uso do espaço.

(A5)
arvore({:p123, :e, {:nao, :q4556}})
    e
p123 nao
     q4556

# Algoritmo desenha_arvore

- Entradas:
  - f: a fórmula
  - x_relativo: a posição x relativa ao ponto em que está
  - y: a posição y
- Se f é atômica, simplemente colocar em x_relativo=0, y=atual
- Se f é {:nao, subf}:
  - colocar "nao" em x_relativo=0, y
  - chamar recursivamente com
    - subf, x_relativo, y+1
- Se f é {subfe, conectivo, subfd}:
   - colocar conectivo em x_relativo+largura(subfe), y
  - chamar recursivamente com
    - subfe, x_relativo, y+1
  - chamar recursivamente com
    - subfd, x_relativo+largura(conectivo)+largura(subfe), y+1

## Fator complicador

Um fator complicador é a largura do texto nas situações em que temos um conectivo unário acima de um binário.

### A largura inicial da fórmula entra em algum ponto???




# Funções já foram implementadas

- largura
  - de um átomo: basta converter para string
  - de uma fórmula: precisa ver as subfórmulas(veja caso A3)
