defmodule LohicTruthTablesTest do
  use ExUnit.Case
  import LohicTruthTables

  test "Valuation of p1 when v(p1)=1 is 1" do
    valuation = %{:p1 => 1}
    assert value(:p1, valuation) == 1
  end

  test "Valuation of !p1 when v(p1)=1 is 0" do
    valuation = %{:p1 => 1}
    assert value({:not, :p1}, valuation) == 0
  end
end
