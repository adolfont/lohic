defmodule LohicSubformulas do
  defp conjunto_subformulas_aux(formula, atual) when is_atom(formula) do
    [formula] ++ atual
  end

  defp conjunto_subformulas_aux(formula = {_, subformula}, atual) do
    conjunto_subformulas_aux(subformula, [formula] ++ atual)
  end

  defp conjunto_subformulas_aux(formula = {subformula_esquerda, _, subformula_direita}, atual) do
    conjunto_subformulas_aux(subformula_esquerda, [formula] ++ atual) ++
      conjunto_subformulas_aux(subformula_direita, [])
  end

  @doc """
  Calcula o conjunto de subfórmulas de uma fórmula.
  """
  def subformulas(formula) do
    MapSet.new(conjunto_subformulas_aux(formula, []))
  end

  @doc """
  Calcula o conjunto de subfórmulas próprias de uma fórmula.
  """
  def conjunto_subformulas_proprias(formula) do
    MapSet.delete(subformulas(formula), formula)
  end

  def is_subformula(formula, x) do
    Enum.member?(subformulas(formula), x)
  end
end
