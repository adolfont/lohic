defmodule LohicArvore do
  import Lohic

  def sublista(_, inicio, fim) when inicio > fim do
    []
  end

  def sublista(lista, inicio, fim) do
    Enum.slice(lista, inicio..fim)
  end

  def substring(_, inicio, fim) when inicio > fim do
    ""
  end

  def substring(string, inicio, fim) do
    String.slice(string, inicio..fim)
  end

  def coloca(matriz, linha, coluna, valor) do
    linha_a_mudar = Enum.at(matriz, linha)
    inicio_nova_linha = substring(linha_a_mudar, 0, coluna - 1)
    fim_nova_linha = String.slice(linha_a_mudar, (coluna + 1)..-1)
    nova_linha = inicio_nova_linha <> valor <> fim_nova_linha

    sublista(matriz, 0, linha - 1) ++ [nova_linha] ++ Enum.slice(matriz, (linha + 1)..-1)
  end

  def colocaVarios(matriz, _, _, "") do
    matriz
  end

  def colocaVarios(matriz, linha, coluna, valor) do
    matriz = coloca(matriz, linha, coluna, String.at(valor, 0))
    colocaVarios(matriz, linha, coluna + 1, String.slice(valor, 1..-1))
  end

  def inicializa(0, _, _) do
    []
  end

  def inicializa(linhas, colunas, caractere) do
    [String.duplicate(caractere, colunas)] ++ inicializa(linhas - 1, colunas, caractere)
  end

  defp colocarConectivoNaPosicao(matriz, linha, coluna, conectivo) do
    matriz
    |> colocaVarios(linha, coluna, conectivo)
  end

  defp calcula_coluna_conectivo_binario(coluna, _, subformula_esquerda) do
    coluna + largura(subformula_esquerda)
  end

  defp calcula_coluna_conectivo_unario(coluna, conectivo, subformula) when is_atom(subformula) do
    coluna + max(0, div(largura(subformula) - largura(conectivo), 2))
  end

  defp calcula_coluna_conectivo_unario(coluna, _, {conectivo_unario, subf}) do
    calcula_coluna_conectivo_unario(coluna, conectivo_unario, subf)
  end

  defp calcula_coluna_conectivo_unario(coluna, conectivo_unario, {subfe, conectivo_binario, _}) do
    posicao_c2 = calcula_coluna_conectivo_binario(coluna, conectivo_binario, subfe)
    largura_c2 = largura(conectivo_binario)
    largura_c1 = largura(conectivo_unario)
    deslocamento = max(0, div(largura_c2 - largura_c1, 2))

    if largura_c2 >= largura_c1 do
      posicao_c2 + deslocamento
    else
      coluna + deslocamento
    end
  end

  defp desenha_conectivo(conectivo) when is_atom(conectivo) do
    Atom.to_string(conectivo)
  end

  defp desenha_conectivo(conectivo) do
    conectivo
  end

  defp desenha_com_dados(matriz, linha, coluna, formula)
       when is_atom(formula) do
    matriz
    |> colocaVarios(linha, coluna, Atom.to_string(formula))
  end

  defp desenha_com_dados(matriz, linha, coluna, {conectivo, subformula}) do
    colocaVarios(
      matriz,
      linha,
      calcula_coluna_conectivo_unario(coluna, conectivo, subformula),
      desenha_conectivo(conectivo)
    )
    |> desenha_com_dados(linha + 1, coluna, subformula)
  end

  defp desenha_com_dados(
         matriz,
         linha,
         coluna,
         {subformula_esquerda, conectivo, subformula_direita}
       ) do
    # conectivo_como_string = Atom.to_string(conectivo)
    #    - colocar conectivo em x_relativo+largura(subfe), y
    #   - chamar recursivamente com
    #     - subfe, x_relativo, y+1
    #   - chamar recursivamente com
    #     - subfd, x_relativo+largura(conectivo)+largura(subfe), y+1
    matriz
    |> colocarConectivoNaPosicao(
      linha,
      coluna + largura(subformula_esquerda),
      desenha_conectivo(conectivo)
    )
    |> desenha_com_dados(linha + 1, coluna, subformula_esquerda)
    |> desenha_com_dados(
      linha + 1,
      coluna + largura(subformula_esquerda) + largura(conectivo),
      subformula_direita
    )
  end

  defp desenho_arvore(formula) do
    inicializa(informacoes_formula(formula).altura, informacoes_formula(formula).largura, " ")
    |> desenha_com_dados(0, 0, formula)
  end

  def informacoes_formula(formula) do
    %{complexidade: complexidade(formula), largura: largura(formula), altura: altura(formula)}
  end

  def arvore(x) do
    desenho_arvore(x)
  end
end
