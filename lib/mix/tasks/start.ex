defmodule Mix.Tasks.Start do
  use Mix.Task
  import Lohic

  @spec parse(binary) :: tuple
  def parse(str) do
    {:ok, tokens, _} = str |> to_charlist() |> :formula_lexer.string()
    :formula_parser.parse(tokens)
  end

  @lpc {[:p, :q, :r, :s, :t], %{:not => 1, :and => 2, :or => 2, :implies => 2}}
  # @pretty_printing_symbols %{:not => "!", :and => "&", :or => "|", :implies => "->"}
  @pretty_printing_symbols %{:not => "¬", :and => "∧", :or => "∨", :implies => "→"}
  @input_symbols %{:not => "!", :and => "&", :or => "|", :implies => "->"}

  def change_connective_symbols(formula) when is_atom(formula) do
    formula
  end

  def change_connective_symbols({unary_connective, subformula}) do
    {@pretty_printing_symbols[unary_connective], change_connective_symbols(subformula)}
  end

  def change_connective_symbols({lsubformula, connective, rsubformula}) do
    {change_connective_symbols(lsubformula), @pretty_printing_symbols[connective],
     change_connective_symbols(rsubformula)}
  end

  def pretty_printing_formula(formula) when is_atom(formula) do
    Atom.to_string(formula)
  end

  def pretty_printing_formula({unary_connective, subformula}) do
    @pretty_printing_symbols[unary_connective] <> pretty_printing_formula(subformula)
  end

  def pretty_printing_formula({lsubformula, connective, rsubformula}) do
    "(" <>
      pretty_printing_formula(lsubformula) <>
      @pretty_printing_symbols[connective] <> pretty_printing_formula(rsubformula) <> ")"
  end

  def atoms_as_string(atoms) do
    for(x <- atoms, do: Atom.to_string(x))
    |> Enum.join(", ")
  end

  def connectives_as_string(connectives) do
    for {x, y} <- connectives do
      "\"" <> @input_symbols[x] <> "\" arity=#{y}"
    end
    |> Enum.join(", ")
  end

  def subformulas_as_string(subformulas) do
    content =
      for x <- subformulas do
        pretty_printing_formula(x)
      end
      |> Enum.join(", ")

    "{" <> content <> "}"
  end

  def show_language({atoms, connective_map}) do
    IO.puts("Atomic formulas allowed: #{atoms_as_string(atoms)}")
    IO.puts("Connectives allowed: #{connectives_as_string(connective_map)}")
  end

  def verify_input do
    try do
      IO.gets("Type a formula: ") |> parse()
    rescue
      _ -> {:error, "It's not a formula!"}
    end
  end

  def show_tree(formula) do
    IO.puts("Sintax tree:")
    IO.puts("")
    for i <- LohicArvore.arvore(change_connective_symbols(formula)), do: IO.puts(i)
    IO.puts("")
  end

  def bye() do
    IO.puts("Bye!")
  end

  def execute_option(option) do
    case Integer.parse(option) do
      {0, _} -> bye()
      {1, _} -> ask_and_show()
      {2, _} -> ask_if_is_subformula()
      {_, _} -> menu()
      :error -> menu()
    end
  end

  def create_a_complex_formula({connective, 1}, size) do
    {connective, create_a_formula_aux(size - 1)}
  end

  def create_a_complex_formula({connective, 2}, size) do
    split = :rand.uniform(size - 1)
    {create_a_formula_aux(size - split), connective, create_a_formula_aux(split)}
  end

  def create_a_formula_aux(1) do
    {atoms, _} = @lpc
    Enum.at(atoms, :rand.uniform(length(atoms) - 1))
  end

  def create_a_formula_aux(size) do
    {_, connectives} = @lpc
    random_connective = Enum.at(connectives, :rand.uniform(length(Map.keys(connectives)) - 1))

    create_a_complex_formula(random_connective, size)
  end

  def create_a_formula() do
    # Tamanho máximo de cada subfórmula fixado!
    max_size = :rand.uniform(10)
    create_a_formula_aux(max_size)
  end

  def select_a_subformula(formula) do
    subformulas = MapSet.to_list(LohicSubformulas.subformulas(formula))
    Enum.at(subformulas, :rand.uniform(length(subformulas)) - 1)
  end

  def change(f, true) when is_atom(f) do
    {atoms, _} = @lpc
    Enum.at(atoms -- [f], :rand.uniform(length(atoms) - 2))
  end

  def change({connective, subformula}, true) do
    {connective, change(subformula, true)}
  end

  def change({lsubformula, connective, rsubformula}, true) do
    choice = :rand.uniform(2)

    if choice == 1 do
      {lsubformula, connective, change(rsubformula, true)}
    else
      {change(lsubformula, true), connective, rsubformula}
    end
  end

  # def change({lsubformula, connective, rsubformula}, true) do
  # TODO: ter um que mude o conectivo???
  # end

  def change(f, false) do
    f
  end

  def check_answer("Yes", true) do
    IO.puts("Correct answer!")
  end

  def check_answer("No", false) do
    IO.puts("Correct answer!")
  end

  def check_answer(_, _) do
    IO.puts("Wrong answer!")
  end

  def ask_if_is_subformula_aux(subformula, formula) do
    # Sorteio com mais chances de não ser
    change_subformula? = :rand.uniform(2) > 1
    changed_subformula = change(subformula, change_subformula?)

    IO.puts(
      "Is #{pretty_printing_formula(changed_subformula)} a subformula of #{
        pretty_printing_formula(formula)
      }?"
    )

    answer = IO.gets("Answer (Yes/No): ")

    check_answer(String.trim(answer), LohicSubformulas.is_subformula(formula, changed_subformula))
  end

  def ask_if_is_subformula() do
    random_formula = create_a_formula()
    # IO.puts(pretty_printing_formula(random_formula))
    subformula = select_a_subformula(random_formula)
    ask_if_is_subformula_aux(subformula, random_formula)
    menu()
  end

  def menu() do
    IO.puts("0 - End")
    IO.puts("1 - Passive learning")
    IO.puts("2 - Is f_1 a subformula of f_2?")
    option = IO.gets("Choose an option: ")
    execute_option(option)
  end

  def ask_and_show do
    {result, formula} = verify_input()

    if result == :ok and formula_bem_formada?(formula, @lpc) do
      IO.puts("The formula is well formed according to the following language:")
      show_language(@lpc)

      IO.puts(
        "Subformulas: #{
          subformulas_as_string(MapSet.to_list(LohicSubformulas.subformulas(formula)))
        }"
      )

      # IO.inspect(MapSet.to_list(LohicSubformulas.subformulas(formula)), label: "Subformulas")
      IO.inspect(Lohic.complexidade(formula), label: "Size")
      show_tree(formula)
    else
      IO.puts("The formula is not well formed according to the following language:")
      show_language(@lpc)
    end

    # message = "Syntax error on #{formula}"
    run(:continue)
  end

  def run(:continue) do
    menu()
  end

  def run(_) do
    IO.puts("Lohic App - 2018")
    IO.puts("Syntax tree for Classical Propositional Logic formulas")
    IO.puts("Propositional language used:")
    show_language(@lpc)

    menu()
  end
end
