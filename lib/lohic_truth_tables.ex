defmodule LohicTruthTables do
  use Bitwise

  def tt_values(values, 1) do
    for x <- values, do: [x]
  end

  def tt_values(values, n) do
    combine(tt_values(values, 1), tt_values(values, n - 1))
  end

  def combine(x, y) do
    for m <- x,
        n <- y,
        do: List.flatten([m, n])
  end

  def value(formula, valuation) when is_atom(formula) do
    valuation[formula]
  end

  def value({:not, subformula}, valuation) do
    1 - value(subformula, valuation)
  end

  def value({left_subformula, :and, right_subformula}, valuation) do
    value(left_subformula, valuation) &&& value(right_subformula, valuation)
  end

  def value({left_subformula, :or, right_subformula}, valuation) do
    value(left_subformula, valuation) ||| value(right_subformula, valuation)
  end

  def value({left_subformula, :implies, right_subformula}, valuation) do
    1 - value(left_subformula, valuation) ||| value(right_subformula, valuation)
  end

  def truth_table(formula, atoms) do
    valuations =
      for x <- tt_values([0, 1], length(atoms)) do
        Enum.zip(atoms, x) |> Enum.into(%{})
      end

    for x <- valuations do
      {x, value(formula, x)}
    end
  end
end
