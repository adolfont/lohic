defmodule Tableaux do
  def pprint({sign, formula}) do
    String.upcase(Atom.to_string(sign)) <> " " <> Mix.Tasks.Start.pretty_printing_formula(formula)
  end

end


IO.puts(Tableaux.pprint({:t, :p}))
IO.puts(Tableaux.pprint({:f, {:p, :and, :q}}))
