defmodule Lohic do
  defp formula_bem_formada_unaria?({conn, subformula}, conectivos, lpc) do
    conectivos[conn] == 1 and formula_bem_formada?(subformula, lpc)
  end

  defp formula_bem_formada_binaria?(
         {subformula_esquerda, conn, subformula_direita},
         conectivos,
         lpc
       ) do
    conectivos[conn] == 2 and formula_bem_formada?(subformula_esquerda, lpc) and
      formula_bem_formada?(subformula_direita, lpc)
  end

  def formula_bem_formada?(x, {atoms, _}) when is_atom(x) do
    Enum.member?(atoms, x)
  end

  def formula_bem_formada?(f = {_, _}, lpc = {_, conectivos}) do
    formula_bem_formada_unaria?(f, conectivos, lpc)
  end

  def formula_bem_formada?(f = {_, _, _}, lpc = {_, conectivos}) do
    formula_bem_formada_binaria?(f, conectivos, lpc)
  end

  def formula_bem_formada?(_, _), do: false

  def complexidade(x) when is_atom(x), do: 1
  def complexidade({x, y}) when is_atom(x), do: 1 + complexidade(y)
  def complexidade({x, y, z}) when is_atom(y), do: 1 + complexidade(x) + complexidade(z)
  def complexidade(_), do: :error

  def altura(x) when is_atom(x) do
    1
  end

  def altura({_, subformula}) do
    1 + altura(subformula)
  end

  def altura({subformula_esquerda, _, subformula_direita}) do
    1 + max(altura(subformula_esquerda), altura(subformula_direita))
  end

  def largura(x) when is_atom(x) do
    String.length(Atom.to_string(x))
  end

  def largura({conectivo, subformula}) when is_atom(conectivo) do
    max(String.length(Atom.to_string(conectivo)), largura(subformula))
  end

  def largura({conectivo, subformula}) do
    max(String.length(conectivo), largura(subformula))
  end

  def largura({subformula_esquerda, conectivo, subformula_direita}) when is_atom(conectivo) do
    String.length(Atom.to_string(conectivo)) + largura(subformula_esquerda) +
      largura(subformula_direita)
  end

  def largura({subformula_esquerda, conectivo, subformula_direita}) do
    String.length(conectivo) + largura(subformula_esquerda) + largura(subformula_direita)
  end

  def largura(x) do
    String.length(x)
  end
end
